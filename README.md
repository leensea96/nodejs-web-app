# Small FullStack Single Page App (SPA)

![alt text](web-app-updated.png)

## Demo Website

You can access the demo website at: **[https://d1tqm374y224ee.cloudfront.net](https://d1tqm374y224ee.cloudfront.net)**

## Architect

This web app is fully hosted on AWS, using following services:

- AWS S3 for React frontend content hosting
- AWS CloudFront for CDN and webpage security config (HTTPS, WAF,...)
- AWS Cognito and AWS IAM for user authentication and authorization
- For backend, I use 2 kind of service: Containerized service hosted on AWS EKS, and serverless service run on AWS Lambda. These services are exposed as APIs through AWS API Gateway.
- For data layer, AWS DynamoDB is used for storing users session info.
