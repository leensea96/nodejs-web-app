var express = require("express");
var router = express.Router();
os = require("os");
//import { createClient } from "redis";
var redis = require("redis");
const fs = require("node:fs");
let password;

router.get("/healthcheck", function (req, res, next) {
  res.render("index", { hostname: os.hostname() });
});

router.get("/", function (req, res, next) {
  res.status(200).redirect("index.html");
});

router.post("/subscribe", function (req, res, next) {
  const new_email = req.body.email;
  console.log(new_email);
  res.redirect("index.html");
});

router.post("/api/redis", async function (req, res, next) {
  //Get user email and question
  const email = req.body.email;
  const question = req.body.question;
  console.log(email);
  console.log(question);
  // get the current date and time as a string
  const now = new Date();
  const currentDateTime = now.toLocaleString();
  console.log(currentDateTime);

  //Set up Redis connection
  try {
    password = fs.readFileSync("/mnt/secrets/REDIS_PASS", "utf8");
  } catch (err) {
    console.error(err);
  }
  const clientRedis = redis.createClient({
    password: password,
    socket: {
      host: "redis-13961.c274.us-east-1-3.ec2.redns.redis-cloud.com",
      port: 13961,
    },
  });
  clientRedis.on("error", (err) => console.log("Redis Client Error", err));
  await clientRedis.connect();
  console.log("connect Redis OK!");
  // console.log(await clientRedis.get("name"));
  await clientRedis.hSet(email, {
    email: email,
    lastLogin: currentDateTime,
    question: question,
  });
  await clientRedis.disconnect();
  res.json({ msg: "Request finished!" });
});

module.exports = router;
